#!/bin/sh

docker build -f Dockerfile -t xelatex-project-faas .
docker run -it -v $PWD/docs:/home -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY  xelatex-project-faas /bin/bash